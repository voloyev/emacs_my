;;; package --- Summary:
;;; Code:
;;; Commentary:
;; haskell module
(use-package intero
    :ensure t
    :init (intero-global-mode 1))

(provide 'haskell-module)
;;; haskell-module.el ends here
