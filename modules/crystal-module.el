;;; package --- Summary:
;;; Code:
;;; Commentary:
;; crystal module

;; crystal mode
(use-package crystal-mode
    :ensure t)

(provide 'crystal-module)
;;; crystal-module.el ends here
