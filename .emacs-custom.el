
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(coffee-tab-width 2)
 '(js-basic-offset 2)
 '(js2-basic-offset 2)
 '(jsx-basic-offset 2)
 '(package-selected-packages
   (quote
    (gorepl-mode minitest exec-path-from-shell ag dockerfile-mode calfw-org nord-theme rainbow-delimiters rainboe-delimiters-mode rainboe-delimiters flx dimmer fixmee racket sly-mode langtool hyde flycheck-crystal lsp-vue lsp-mode lsp-rust vi-tilde-fringe ivy-mode highlight-symbol highlight-indentation-mode highlight-indentation-current-column-mode highlight-indent-guides crystal-mode eink-theme nlinum-relative pocket-reader grayscale-theme textmate geiser blank-mode dump-jump dump-jump-mode tldr git-timemachine paredit parinfer fiplr beacon ztree dashboard workgroups2 ruby-factory dumb-jump eredis bookmark+ bm god-mode projectile-hanami persp-projectile persp-mode vlf realgud-pry elpy counsel counsel-gtags counsel-projectile ivy nginx-mode flyspell-popup toggle-quotes bundler use-package sexy-monochrome-theme coffee-mode alchemist yard-mode enh-ruby-mode vmd-mode ruby-refactor ruby-test-mode vimrc-mode switch-window feature-mode gitconfig gitignore-mode zenburn-theme yari yaml-mode which-key weechat web-mode wanderlust vimish-fold toml-mode toml thrift systemd stylus-mode ssh sqlup-mode sqlplus sqlite sql-indent smarty-mode smartparens smart-mode-line sly-company slim-mode skewer-mode scss-mode sass-mode rvm ruby-tools ruby-hash-syntax ruby-dev ruby-block ruby-additional rubocop rspec-mode rsense robe rjsx-mode rinari realgud-rdb2 realgud-byebug react-snippets rbenv ranger rainbow-mode racket-mode racer quickrun pyenv-mode-auto pydoc projectile-variable projectile-speedbar projectile-rails projectile-codesearch phoenix-dark-mono-theme password-store paradox pallet org-page nyan-mode nlinum nim-mode neotree nav migemo markdown-mode magit jsx-mode json-mode js3-mode js2-refactor js2-highlight-vars js2-closure jira jenkins jekyll-modes jdee indium imenu-list imenu-anywhere ibuffer-vc ibuffer-tramp ibuffer-rcirc ibuffer-projectile ibuffer-git highlight-indentation helm-swoop helm-projectile helm-git-grep helm-ag haskell-mode google-c-style golint golden-ratio gitconfig-mode git-gutter-fringe gist ggtags flymd flycheck-rust flycheck-nim flycheck-elixir fill-column-indicator expand-region evil emmet-mode elscreen elixir-yasnippets ein dired+ d-mode ctags-update csv-mode company-web company-tern company-restclient company-quickhelp company-php company-lua company-jedi company-inf-ruby company-go common-lisp-snippets commander clojure-mode cask-mode cargo calfw brainfuck-mode avy arch-packer angular-mode)))
 '(paradox-github-token t t)
 '(pdf-view-midnight-colors (quote ("#DCDCCC" . "#383838")))
 '(sml/no-confirm-load-theme 1)
 '(speedbar-show-unknown-files t)
 '(tool-bar-mode nil)
 '(vc-annotate-background "#2B2B2B")
 '(vc-annotate-color-map
   (quote
    ((20 . "#BC8383")
     (40 . "#CC9393")
     (60 . "#DFAF8F")
     (80 . "#D0BF8F")
     (100 . "#E0CF9F")
     (120 . "#F0DFAF")
     (140 . "#5F7F5F")
     (160 . "#7F9F7F")
     (180 . "#8FB28F")
     (200 . "#9FC59F")
     (220 . "#AFD8AF")
     (240 . "#BFEBBF")
     (260 . "#93E0E3")
     (280 . "#6CA0A3")
     (300 . "#7CB8BB")
     (320 . "#8CD0D3")
     (340 . "#94BFF3")
     (360 . "#DC8CC3"))))
 '(vc-annotate-very-old-color "#DC8CC3")
 '(vue-basic-offset 2))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
